# mixed-submodules-test

This is a test repository used by [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner).

It consists of several submodules to be checked out in various different ways (scheme/protocol used, private/public etc).

